package literaturerec;

import java.io.IOException;
import java.net.MalformedURLException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import synonym.SynonymUtil;
import uk.ac.shef.dcs.oak.jate.JATEException;
import uk.ac.shef.dcs.oak.jate.test.RIDFKeywordExtractor;

import com.skjegstad.utils.BloomFilter;

import cue.lang.stop.StopWords;
import dao.MySQLDao;

public class LiterRecommendation {
	
	static Random r = new Random();
    private static BloomFilter<String> instance = new BloomFilter<String>(0.0001, 5000);
    private static RIDFKeywordExtractor ridfke = new RIDFKeywordExtractor();
	
    private static MySQLDao mysqldao = new MySQLDao();
	static {
		try {
			mysqldao.prepareConnection();
			mysqldao.con.setAutoCommit(false);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static Map<String, String> getfixsizeRecords(int start, int offset) throws MalformedURLException{
		Map<String, String> contentlist = new HashMap<String, String>();
		MySQLDao csd = null;
		try {
			csd = new MySQLDao();
			csd.prepareConnection();
			csd.con.setAutoCommit(false);
			csd.prepareConnection();
			csd.ps = csd.con.prepareStatement("select doi, title, keyword, abstract from articlecitation limit " + start + "," + offset);
			ResultSet rs = csd.ps.executeQuery();
			while (rs.next()) {
				String doi = rs.getString("doi");
				String title = rs.getString("title");
				String keyword = rs.getString("title");
				String abstracts = rs.getString("abstract");
				contentlist.put(doi, title + " " + keyword + " " + abstracts);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			csd.close();
		}
		return contentlist;
	}
	
	public static void main(String[] args) throws JATEException, IOException {
		try {
			Map<String, String> contentlist = null;
			int counter = 0;
			while (true) {
				contentlist = LiterRecommendation.getfixsizeRecords(counter, 1000);
				if (contentlist.size() <= 0) {
					System.out.println("数据处理结束");
					break;
				}
				Set<String> key = contentlist.keySet();
				for (Iterator it = key.iterator(); it.hasNext();)  {
					String doi = (String) it.next();
					List<String> kwlist = ridfke.extract(contentlist.get(doi));
					String synsetstr = "";
					for (String keyword : kwlist) {
						keyword = keyword.toLowerCase();
						if (!StopWords.English.isStopWord(keyword)) {
							synsetstr += SynonymUtil.getSynonymSet(keyword);
						}
					}
					System.out.println("================ " + counter + " ================");
					System.out.println(kwlist);
					System.out.println(synsetstr);
					mysqldao.updatearticlecition(doi, kwlist, synsetstr);
					kwlist = null;
					counter++;
				}
				contentlist = null;
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
	}

}
